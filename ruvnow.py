#!/usr/bin/env python
# -*- coding: utf-8 -*-

import validators
import requests, os, re, urllib2, sys, m3u8, shutil, logging
from datetime import datetime, date
from bs4 import BeautifulSoup
from urlparse import urlparse   
from ffmpy import FFmpeg

#Configure logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
# create a file handler
handler = logging.FileHandler('ruvnow.log')
handler.setLevel(logging.DEBUG)
# create console handler
consoleHandler = logging.StreamHandler()
consoleHandler.setLevel(logging.DEBUG)
# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
consoleHandler.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(handler)
logger.addHandler(consoleHandler)

#Global variable
tempDirectory = "temp"

def tryint(s):
    try:
        return int(s)
    except:
        return s

def alphanum_key(s):
    """ Turn a string into a list of string and number chunks.
        "z23a" -> ["z", 23, "a"]
    """
    return [ tryint(c) for c in re.split('([0-9]+)', s) ]
    
def get_media_url(page_url):
    """
    Find the url to the MP4/MP3 on a page

    :param page_url: Page to find the url on
    :return: url
    """

    doc = get_document(page_url).prettify()
    
    media_url = None
    media_url_base = None

    urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', doc)
    for url in urls:
        if url.endswith('m3u8'):
            media_url = url
            media_url_base = media_url[0:media_url.rfind('/')]
            break
            
    return media_url, media_url_base
    
def get_document(url):
    """
    Downloads url and returns a BeautifulSoup object

    :param url: Any url
    :return BeautifulSoup "document"
    """
    req = requests.get(url)
    doc = BeautifulSoup(req.content, "html.parser")
    
    return doc
    
def download_ts_files(media_url, media_url_base):
    #Parse the m3u8 file
    logger.info("Parsing m3u8 file at %s" % media_url)
    m3u8_obj = m3u8.load(media_url)  
    
    logger.info("No. of segments in m3u8 is %s" % (len(m3u8_obj.segments)))
    i = 1
    
    #Loop through all the segments
    for segment in m3u8_obj.segments:        
        
        #Create the url to the segment
        segment_url = "%s/%s" % (media_url_base,segment.uri)
        
        #Create filename
        filePath = os.path.join(tempDirectory,segment.uri)
        
        #Download file
        logger.info("Downloading segment number %s (%s)" % (i,segment_url))
        f = urllib2.urlopen(segment_url)
        data = f.read()
        with open(filePath, "wb") as code:
            code.write(data)
        
        #Increment counter
        i += 1

def merge_ts_files(merged_file_path):
    #Get list of all ts files and perform "natural sorting" or "human sorting" (since the default is lexicographical sorting)
    ts_files = [file for file in os.listdir(tempDirectory) if file.endswith('.ts')]
    ts_files.sort(key=alphanum_key)
    
    logger.info("No. of segment files on disk is %s." % len(ts_files) )

    #Open one ts_file from the list after another and append them to merged.ts
    with open(merged_file_path, 'wb') as merged:
        for ts_file in ts_files:
            ts_file_path = os.path.join(tempDirectory,ts_file)
            
            logger.info("Merging segment file %s" % ts_file_path)
            
            with open(ts_file_path, 'rb') as mergefile:
                shutil.copyfileobj(mergefile, merged)        

def ffmpeg_ts_to_mp4(ts_filePath, filePath):
    
    try:
        logger.info("Converting TS content (%s) to MP4 (%s)" % (ts_filePath, filePath))
        ff = FFmpeg(inputs={ts_filePath: None}, outputs={filePath: None})
        ff.run()
    except Exception as ex:
        logger.error("Unable to convert TS file %s to MP4 file %s" % (ts_filePath,filePath))
        logger.error("Error is: %s" % ex)

def frettir():
    #Download directory
    downloadDirectory = "Frettir" 
    
    #Check if directory for Frettir exists    
    if os.path.isdir(downloadDirectory) is False:
        os.mkdir(downloadDirectory) 
        logger.info("Creating download directory (%s)" % downloadDirectory)
    
    #Check if directory for Frettir exists    
    if os.path.isdir(downloadDirectory) is False:
        os.mkdir(downloadDirectory) 
        logger.info("Creating download directory (%s)" % downloadDirectory)
    
    #Date object represents the day containint the content to be downloaded. Defaults to today's date
    date_obj = date.today()
    
    #Get today's date in the correct format for URL
    targetFileName = date_obj.strftime('Frettir_%d_%b_%Y.mp4')
    ts_fileName = date_obj.strftime('Frettir_%d_%b_%Y.ts')
    logger.info("Target file name is %s" % targetFileName)
    
    #Create path to to store the concatenated file
    targetFilePath = os.path.join(downloadDirectory,targetFileName)
    ts_filePath = os.path.join(tempDirectory,ts_fileName)
    logger.info("Target file path is %s" % targetFilePath)    
    
    #Get today's date in the correct format for URL
    today = date_obj.strftime('%Y%m%d')
    
    #Create the url
    page_url = "http://ruv.is/sarpurinn/ruv/frettir/%s" % today
    
    #If we don't already have the file, try and download it
    if os.path.isfile(targetFilePath) is False:
        #Get the file from ruv.is
        media_url, media_url_base = get_media_url(page_url)
        logger.info("media_url is %s" % media_url)
        logger.info("media_url_base is %s" % media_url_base)
        
        #If a url was found, continue.
        if media_url is not None:
            
            logger.info("Downloading Frettir for %s" % date_obj)        
            
            #Download ts files.
            download_ts_files(media_url, media_url_base)
            
            logger.info("Merge ts files into %s" % ts_filePath) 
            
            #Merge all ts files
            merge_ts_files(ts_filePath)
            
            #Finally, convert the MPEG transport stream to MP4 since PLEX does not support TS.
            ffmpeg_ts_to_mp4(ts_filePath, targetFilePath)
        else:
            logger.error("Cannot start download. media_url was None")
    else:
        logger.info("Not starting download for Frettir. %s already exists." % targetFilePath)

def kastljos():
    #Download directory
    downloadDirectory = "Kastljos" 
    
    #Check if directory exists    
    if os.path.isdir(downloadDirectory) is False:
        os.mkdir(downloadDirectory) 
        logger.info("Creating download directory (%s)" % downloadDirectory)
    
    #Date object represents the day containint the content to be downloaded. Defaults to today's date
    date_obj = date.today()
    
    #Get today's date in the correct format for URL
    targetFileName = date_obj.strftime('Kastljos_%d_%b_%Y.mp4')
    ts_fileName = date_obj.strftime('Kastljos_%d_%b_%Y.ts')
    logger.info("Target file name is %s" % targetFileName)
    
    #Create path to to store the concatenated file
    targetFilePath = os.path.join(downloadDirectory,targetFileName)
    ts_filePath = os.path.join(tempDirectory,ts_fileName)
    logger.info("Target file path is %s" % targetFilePath)    
    
    #Get today's date in the correct format for URL
    today = date_obj.strftime('%Y%m%d')
    
    #Create the url
    page_url = "http://ruv.is/sarpurinn/ruv/kastljos/%s" % today
    
    #If we don't already have the file, try and download it
    if os.path.isfile(targetFilePath) is False:
        #Get the file from ruv.is
        media_url, media_url_base = get_media_url(page_url)
        logger.info("media_url is %s" % media_url)
        logger.info("media_url_base is %s" % media_url_base)
        
        #If a url was found, continue.
        if media_url is not None:
            
            logger.info("Downloading Kastljos for %s" % date_obj)        
            
            #Download ts files.
            download_ts_files(media_url, media_url_base)
            
            logger.info("Merge ts files into %s" % ts_filePath) 
            
            #Merge all ts files
            merge_ts_files(ts_filePath)
            
            #Finally, convert the MPEG transport stream to MP4 since PLEX does not support TS.
            ffmpeg_ts_to_mp4(ts_filePath, targetFilePath)
        else:
            logger.error("Cannot start download. media_url was None")
    else:
        logger.info("Not starting download for Kastljos. %s already exists." % targetFilePath)

def download_url_ruv(page_url):
    
    #Get the file from ruv.is
    media_url, media_url_base = get_media_url(page_url)
    logger.info("media_url is %s" % media_url)
    logger.info("media_url_base is %s" % media_url_base)
    
    logger.info("Downloading from url %s" % page_url)        
    
    #Enter file name to save
    file_name = raw_input("Please enter a filename to save the download to: ")
    
    #Download directory
    downloadDirectory = "Misc"
    if os.path.isdir(downloadDirectory) is False:
        os.mkdir(downloadDirectory) 
        logger.info("Creating download directory (%s)" % downloadDirectory)
    
    #Create file paths
    ts_filePath = os.path.join(tempDirectory,"%s.ts" % file_name)
    targetFilePath = os.path.join(downloadDirectory,"%s.mp4" % file_name)
    
    #Download ts files.
    download_ts_files(media_url, media_url_base)
    
    logger.info("Merge ts files into %s" % ts_filePath) 
    
    #Merge all ts files
    merge_ts_files(ts_filePath)
    
    #Finally, convert the MPEG transport stream to MP4 since PLEX does not support TS.
    ffmpeg_ts_to_mp4(ts_filePath, targetFilePath)

def download_url_stod2(page_url):
    
    #Get the file from ruv.is
    media_url = page_url
    media_url_base = media_url_base = media_url[0:media_url.rfind('/')]
    
    logger.info("media_url is %s" % media_url)
    logger.info("media_url_base is %s" % media_url_base)
    
    logger.info("Downloading from url %s" % page_url)
    
    #Enter file name to save
    file_name = raw_input("Please enter a filename to save the download to: ")
    
    #Download directory
    downloadDirectory = "Stod2"
    if os.path.isdir(downloadDirectory) is False:
        os.mkdir(downloadDirectory) 
        logger.info("Creating download directory (%s)" % downloadDirectory)
    
    #Create file paths
    ts_filePath = os.path.join(tempDirectory,"%s.ts" % file_name)
    targetFilePath = os.path.join(downloadDirectory,"%s.mp4" % file_name)
    
    #Download ts files.
    download_ts_files(media_url, media_url_base)
    
    logger.info("Merge ts files into %s" % ts_filePath) 
    
    #Merge all ts files
    merge_ts_files(ts_filePath)
    
    #Finally, convert the MPEG transport stream to MP4 since PLEX does not support TS.
    ffmpeg_ts_to_mp4(ts_filePath, targetFilePath)

def clear_temp_dir():
    #Cleanup: Remove temp directory
    if os.path.isdir(tempDirectory):
        shutil.rmtree(tempDirectory)
        os.mkdir(tempDirectory)
        logger.info("Removing and re-creating temp directory (%s)" % tempDirectory)
    else:
        os.mkdir(tempDirectory)
        logger.info("Creating temp directory (%s)" % tempDirectory)

def stod2():
    
    #Download directory
    downloadDirectory = "Stod2"
    
    #Check if directory for Frettir exists    
    if os.path.isdir(downloadDirectory) is False:
        os.mkdir(downloadDirectory) 
        logger.info("Creating download directory (%s)" % downloadDirectory)
    
    #Date object represents the day containint the content to be downloaded. Defaults to today's date
    date_obj = date.today()
    
    #Get today's date in the correct format for URL
    targetFileName = date_obj.strftime('Stod2_Frettir_%d_%b_%Y.mp4')
    ts_fileName = date_obj.strftime('Stod2_Frettir_%d_%b_%Y.ts')
    logger.info("Target file name is %s" % targetFileName)
    
    #Create path to to store the concatenated file
    targetFilePath = os.path.join(downloadDirectory,targetFileName)
    ts_filePath = os.path.join(tempDirectory,ts_fileName)
    logger.info("Target file path is %s" % targetFilePath)
    
    #Assemble date information in the correct format for URL
    year = date_obj.strftime('%Y')
    month = date_obj.strftime('%m')
    day = date_obj.strftime('%d')
    
    #If we don't already have the file, try and download it
    if os.path.isfile(targetFilePath) is False:
        #Prepare media_url for visir.is
        media_url = "http://visirvod.365cdn.is/hls-vod/_definst_/mp4:SRC/VEFTVMP4/sources/kvoldfrettir/%s_%s/kvoldfrettir_%s_%s_%s.mp4/chunklist.m3u8" % (year,month,year,month,day)
        media_url_base = "http://visirvod.365cdn.is/hls-vod/_definst_/mp4:SRC/VEFTVMP4/sources/kvoldfrettir/%s_%s/kvoldfrettir_%s_%s_%s.mp4" % (year,month,year,month,day)
        
        logger.info("media_url is %s" % media_url)
        logger.info("media_url_base is %s" % media_url_base)
        
        #If a url was found, continue.
        if media_url is not None:
            
            logger.info("Downloading Stod2 Frettir for %s" % date_obj)  
            
            try:                  
                #Download ts files.
                download_ts_files(media_url, media_url_base)
                
                logger.info("Merge ts files into %s" % ts_filePath) 
                
                #Merge all ts files
                merge_ts_files(ts_filePath)
            
                #Finally, convert the MPEG transport stream to MP4 since PLEX does not support TS.
                ffmpeg_ts_to_mp4(ts_filePath, targetFilePath)
            except:
                logger.error("Unable to download. media_url was likely None in reality.")
        else:
            logger.error("Cannot start download. media_url was None")
    else:
        logger.info("Not starting download for Stod2 Frettir. %s already exists." % targetFilePath)

if __name__ == "__main__":    
    
    logger.info('RUVNow starting')    
        
    #Create temporary working directory. Remove it if it exists already   
    clear_temp_dir()
    
    #Check for command line arguments, it is possible to override the defaults and fetch a particular item from Sarpurinn.    
    args = [arg.split('=') for arg in sys.argv]
    
    print args
    
    if len(args) == 3 and len(args[1]) == 2 and args[1][0] == "source" and args[1][1] in ['ruv','stod2'] and args[2][0] == "url" and validators.url(args[2][1]):
        #Download the content on the page        
        if args[1][1] == "ruv":
            download_url_ruv(args[2][1])
        elif args[1][1] == "stod2":
            download_url_stod2(args[2][1])
    elif len(args) == 1: 
    
        #Get Frettir
        frettir()
        clear_temp_dir()
        
        #Get Kastljos
        kastljos()
        clear_temp_dir()
        
        #Stod 2
        stod2()
        clear_temp_dir()
        
        
    logger.info('RUVNow stopping')
          
             