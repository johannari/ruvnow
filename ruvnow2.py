#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
from datetime import datetime
from bs4 import BeautifulSoup
import re, sys

url = 'http://apis.is/tv/ruv'

#for response in responses:
#    print response
#    print "BREAK"
    
#    title = response["title"]
#    print "t: %s" % title
    
#    if title == "Fréttir":
#        print "found"
#        sys.exit(0)
    
    
    #for key in response:
    #    date_object = datetime.strptime(response["startTime"], '%Y-%m-%d %H:%M:%S')    

    
def get_media_url(page_url):
    """
    Find the url to the MP4/MP3 on a page

    :param page_url: Page to find the url on
    :return: url
    """

    doc = get_document(page_url).prettify()
    
    media_url = None

    urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', doc)
    for url in urls:
        if url.endswith('m3u8'):
            media_url = url
            break

    return media_url
    
def get_document(url):
    """
    Downloads url and returns a BeautifulSoup object

    :param url: Any url
    :return BeautifulSoup "document"
    """
    req = requests.get(url)
    doc = BeautifulSoup(req.content, "html.parser")
    
    return doc

if __name__ == "__main__":
    
    responses = requests.get(url).json()
    
    for response in responses["results"]:
        title = response["title"]
        
        if title == u'Fréttir':
            print "found it"
            print response
            
            #Extract date
            startTime = datetime.strptime(response["startTime"], '%Y-%m-%d %H:%M:%S') 
            print startTime
            
            startDate = "%s%s%s" % (startTime.year,startTime.month,startTime.day)
            print startDate
            print get_media_url("http://ruv.is/sarpurinn/ruv/frettir/20161203")
        
        #print title
    
    #print get_media_url("http://ruv.is/sarpurinn/ruv/frettir/20161125")